Feature: Playing BlackJack
The player starts with 2 cards
The player has two options during his turn: hit or stand
If the player decides to hit, he receives a new card
If the player decides to stand, the game is over
When the player has more than 21 points, the game is automatically over and the player has lost
When the game is over and the player has 21 or less points, the computer calculates who has won
Calculation is done as follow:
- Computer gets a random endscore between 16 and 21
- the player must reach a higher endscore to win the game

The endscore of the player is a sum of all the cards in his hands

The card scores are:
7: 7 points
8: 8 points
9: 9 points
10: 10 points
jack: 10 points
queen: 10 points
king: 10 points
ace: 1 or 11 points
