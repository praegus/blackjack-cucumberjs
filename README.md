# blackjack-player

Hi,

Welcome to our amazing blackjack web application.
We have already setup Babel, Webpack, Vue and CSS for you.
But oh no! The business logic is still missing! Which is the most important part of any application.

Luckily, the parameters and methods in src/modules/game.js are used in our frontend. So any change in those parameters is directly shown in the UI.
So if you would use the parameters and methods in game.js to make the game work, you will be rewarded with a nifty looking blackjack app!

Oh and please do this the right way (the BDD way)!
You can find the requirements in the playing_blackjack feature file.

Thanks!


## Setting up Gitlab Runners

Install gitlab-runner
https://docs.gitlab.com/runner/install/

Start new agent
```
gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token ########## \
  --executor shell \
  --tag-list node \
  --description "Simple Node Runner"
```

Config file
https://docs.gitlab.com/runner/configuration/advanced-configuration.html

## Script Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# test scripts
npm run test

# coverage
npm run coverage
```

